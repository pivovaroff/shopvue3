import { createStore } from 'vuex'

const store = createStore({
    state: {
       cart:[]
     },
     mutations: {
      //чи можу я в цій функції закинути перевірку
         setCart: (state, product) => state.cart.push(product),
     },
     getters: {
         getCart: (state) =>
         {
             return state.cart
         }
     }
 })
export default store;


